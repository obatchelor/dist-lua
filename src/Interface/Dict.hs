module  Interface.Dict 
  ( remoteTable
   
  ) where
 

import Control.Distributed.Process.Serializable 
import Control.Distributed.Process 
 
import qualified Data.ByteString.Lazy as L
import qualified Data.ByteString as B
 
import Scripting
 
import Control.Distributed.Process.Dict hiding (remoteTable)

deriveDicts [''LuaCode, ''LuaBinary]


remoteTable :: RemoteTable -> RemoteTable
remoteTable = __remoteTableDecl   