{-# LANGUAGE FlexibleContexts, ScopedTypeVariables #-}

module Interface.Internal where

import Scripting
import qualified Scripting.Lua as Lua

import Backend.SSH (NodeHost(..))  
import qualified Backend.SSH as SSH
import qualified Backend.Local as SSH

import Control.Distributed.Process  
import qualified Control.Exception as E

import Control.Monad.Reader hiding (forM_)
import Data.Foldable (forM_)

import Control.Distributed.Process.Internal.Types (runLocalProcess, NodeId, Process(..), LocalProcess, LocalNode, remoteTable, processNode)
import Control.Distributed.Process.Closure  
import Control.Distributed.Static

import Control.Distributed.Process.Node (runProcess, newLocalNode, initRemoteTable, forkProcess)
import Control.Distributed.Process.Serializable  


import Control.Distributed.Process.Dict (staticApplyEnv)
import qualified Control.Distributed.Process.Dict as Dict

import Network.Transport.TCP (createTransport, defaultTCPParameters)
 
import Control.Lens

import qualified Data.ByteString.Lazy as BL

import Data.Int
import Data.Binary
import Data.IORef

import System.FilePath
import System.Directory
import System.Environment ( getExecutablePath )

import qualified Data.Vector.Storable as V

import Control.Concurrent.MVar

import qualified Network.Info as N   

import qualified Data.Map as M
import Data.Aeson.TH

import Data.ByteString (ByteString)
import Text.Printf

import Interface.Dict ()
  
   


epParamsFields :: [(String, Field SSH.EndPointParams, Bool)]
epParamsFields = allOptional $ 
  [ ("userName",    Field SSH.sshUserName)
  , ("publicKey",   Field SSH.sshPublicKey)
  , ("privateKey",  Field SSH.sshPrivateKey)
  , ("passphrase",  Field SSH.sshPassphrase)
  , ("knownHosts",  Field SSH.sshKnownHosts)
  , ("timeout",     Field SSH.sshTimeout)
  ]
  

  
spawnParamsFields :: [(String, Field SSH.SpawnParams, Bool)]
spawnParamsFields = allOptional $
  [ ("exePath",     Field SSH.spExePath)
  , ("files",       Field SSH.spFiles)
  , ("loggerPid",   Field (SSH.spLoggerPid . mapping _hsBinary))
  , ("libPath",     Field SSH.spLibPath)
  , ("niceLevel",   Field SSH.spNiceLevel)
  , ("kill",        Field SSH.spKill)
  ]  
  

epFields :: [(String, Field SSH.EndPoint, Bool)]
epFields = allRequired $
  [ ("ip",         Field SSH.sshIp)
  , ("port",       Field SSH.sshPort)
  , ("params",     Field SSH.epParams)
  ]  
  

 
instance Lua.StackValue SSH.EndPointParams where
  push = pushRecord epParamsFields 
  peek = peekRecordDefault epParamsFields SSH.defaultSSHParams 
    
    
instance Lua.StackValue SSH.SpawnParams where
  push = pushRecord spawnParamsFields 
  peek =  peekRecordDefault spawnParamsFields SSH.defaultSpawnParams    
    

instance Lua.StackValue SSH.EndPoint where
  push = pushRecord epFields
  peek= peekRecordDefault epFields defaultEp where
    
    defaultEp = do
      sp <- SSH.defaultSSHParams  
      return $ SSH.SSHEndPoint "localhost" 1000 sp
    
    
    
instance Lua.StackValue ProcessId where
  push = pushBinary 
  peek = peekBinary
   
instance Lua.StackValue NodeId where
  push = pushBinary 
  peek = peekBinary
    
    
getInterfaces :: IO (M.Map String String)
getInterfaces = do
  interfaces <- N.getNetworkInterfaces 
  return (M.fromList $ toAssocs interfaces)

  where
    toAssocs interfaces = map (\i -> (N.name i, show $ N.ipv4 i)) $ interfaces 
  


doIdentity :: LuaBinary -> IO LuaBinary
doIdentity (LuaBinary b) = print b >> return (LuaBinary b)  

codeIdentity :: LuaCode -> IO LuaCode
codeIdentity (LuaCode b) = print b >> return (LuaCode b)  

   
stackIdentity :: StackIndex -> IO StackIndex
stackIdentity (StackIndex b) = print b >> return (StackIndex b)  
   
   


  
  
rethrowProcess :: LocalNode -> Process () -> IO ()
rethrowProcess node proc = do
  resultVar <- newEmptyMVar
  void $ forkProcess node $
    (try proc >>= liftIO . putMVar resultVar)
  result <- takeMVar resultVar
  case result of 
    Left  (e :: E.SomeException) -> E.throwIO e
    Right ()                     -> return ()

    
scanHosts :: SSH.EndPointParams -> String -> Int -> IO [SSH.EndPoint]
scanHosts = SSH.portScan
  
  
testHosts :: SSH.EndPointParams -> [String] -> Int -> IO [SSH.EndPoint]
testHosts params hostIps sshPort = SSH.filterConnections (map makeEp hostIps) 
  where
    makeEp ip = SSH.SSHEndPoint ip sshPort params  
 
 
localSpawnParams :: IO SSH.SpawnParams 
localSpawnParams = do

  params <- SSH.defaultSpawnParams
  dir <- getCurrentDirectory
  self <- getExecutablePath
  
  return $ params & SSH.spWorkingPath .~ dir
                  & SSH.spExePath .~ (self, self)

   
  
  
 
spawnParams :: FilePath -> [FilePath] -> IO SSH.SpawnParams 
spawnParams luaPath files = do
  
  extraLibs <- getFiles "./lib" 
  params <- SSH.defaultSpawnParams
 
  let pairs = map (\f -> (f, f)) (files ++ extraLibs)
  return $ params & SSH.spFiles .~ pairs
                  & SSH.spLibPath .~ ["./lib", luaPath]
 
 
updateFiles :: [SSH.EndPoint] ->  [FilePath] -> IO ()
updateFiles eps files = do
  params <- spawnParams "" files
  SSH.prepareFiles $ map (SSH.makeHost params 0) eps

    
   
getFiles :: FilePath -> IO [FilePath]
getFiles dir = do
  contents <- getDirectoryContents dir
  let relative = map (dir </>) contents 
    
  filterM doesFileExist relative    
  
 
type LuaCall = (Maybe LuaCode, [LuaBinary], LuaCode)
 
type SpawnClosure = Static (LuaCall -> Process ())


        
nodeInterface :: SpawnClosure -> LocalProcess -> LuaState -> M.Map String Function
nodeInterface sc env l = M.fromList $ 
  [ ("spawnLocal",  Function $ luaSpawnLocal)
  , ("spawn",       Function $ luaSpawn)
  , ("spawnMany",    Function $ luaSpawnMany)
  , ("send",        Function $ luaSend)
  , ("exit",        Function $ luaExit)  
  , ("kill",        Function $ wrapProcess2 kill)
  , ("expect",      Function $ luaExpect)
  , ("pid",         Function $ wrapProcess getSelfPid)
  , ("node",         Function $ wrapProcess getSelfNode)
  , ("say",         Function $ luaSay)
  , ("spawnNodes",  Function $ luaSpawnNodes)
  , ("spawnLocalNodes",  Function $ luaSpawnLocalNodes)
  ]
  
  where
    
    wrapProcess :: Process a -> IO a
    wrapProcess proc = runLocalProcess env proc 
    
    wrapProcess1 :: (a -> Process b) -> a -> IO b
    wrapProcess1 proc a =  runLocalProcess env (proc a)

    wrapProcess2 :: (a -> b -> Process c) -> a -> b -> IO c
    wrapProcess2 proc a b = runLocalProcess env (proc a b)
    
    luaSpawnLocal :: Maybe LuaCode -> [LuaBinary] -> LuaCode -> IO ProcessId
    luaSpawnLocal setup args code = wrapProcess $ spawnLocal $ 
      runLuaProcess' sc  (setup, args, code)
      
    luaSpawn :: NodeId -> Maybe LuaCode -> [LuaBinary] -> LuaCode -> IO ProcessId
    luaSpawn node setup args code = wrapProcess $ do 
        pid <- spawn node closure  
        send pid sc
        return pid
        
      where
        closure = sc `staticApplyEnv` (setup, args, code)
        
    luaAsync :: Maybe LuaCode -> [LuaBinary] -> LuaCode -> IO [LuaBinary]
    luaAsync = undefined
        
    luaSpawnMany :: [NodeId] -> Maybe LuaCode ->  [[LuaBinary]] -> LuaCode -> IO [ProcessId]
    luaSpawnMany nodes setup args code = zipWithM (\node arg -> luaSpawn node setup arg code) nodes args
     

    luaSpawnLocalNodes ::  Int ->  SSH.EndPointParams -> Int -> StackIndex -> IO ()
    luaSpawnLocalNodes n epParams port f = wrapProcess $ do
      params <- liftIO localSpawnParams
            
      SSH.withLocalNodes hosts params $ spawnRun f
      
      where
        hosts = map ("localhost",) [port..port + n - 1]
        
     
    luaSpawnNodes ::  FilePath ->  Int -> [SSH.EndPoint] -> Int -> StackIndex -> IO ()
    luaSpawnNodes luaPath n eps port f = wrapProcess $ do
      params <- liftIO $ spawnParams luaPath []
      
      let hosts = map ((, n) . SSH.makeHost params port) eps
      SSH.withNodes hosts $ spawnRun f
      
    spawnRun :: StackIndex -> [NodeId] -> Process ()
    spawnRun (StackIndex i) nodes = do      
      (Just logger) <- whereis "logger"
      
      forM nodes $ \node -> do
        reregisterRemoteAsync node "logger" logger
        
      replicateM (length nodes) (expect :: Process RegisterReply)
      
      liftIO $ do
        Lua.pushvalue l i
        Lua.push l nodes
        tryCall l 1 0
        return ()    
      
   
    luaSay :: StackIndex -> IO ()
    luaSay (StackIndex i) = do
      s <- toString l i
      wrapProcess $ say s
      
    luaExit :: ProcessId -> String -> IO ()
    luaExit p s = wrapProcess $ exit p s
 
    luaSend :: ProcessId -> LuaBinary -> IO ()
    luaSend pid value =  wrapProcess $ send pid value      

    luaExpect :: IO LuaBinary
    luaExpect = wrapProcess  expect
    
    

runLuaProcess' :: SpawnClosure -> LuaCall -> Process ()
runLuaProcess' sc call = do
  env <- ask
  liftIO $ runLuaProcess sc env call
    

withLua :: RemoteTable -> SpawnClosure -> (LuaState -> IO ()) -> IO ()
withLua rt sc run = do
  l <- Lua.newstate
  Lua.openlibs l
  loadInterface rt sc l 
  run l
  Lua.close l      
  
callCode :: LuaState -> LuaCode -> [LuaBinary] -> IO ()
callCode l code args = do
  
  Lua.push l code
  forM_ args $ \arg -> Lua.push l arg
  tryCall l (length args) 0 
  
runLuaProcess :: SpawnClosure -> LocalProcess -> LuaCall -> IO ()
runLuaProcess sc env (setup, args, code) = withLua (remoteTable (processNode env)) sc $ \l -> do 
  loadNodeInterface sc env l

  forM_ setup $ \setup' -> callCode l setup' []
  callCode l code args
  

  
        
spawnLocalNode ::  RemoteTable -> SpawnClosure -> LuaState -> String -> Int -> StackIndex -> IO ()
spawnLocalNode rt sc l ip port (StackIndex i) = do
  t <- createTransport ip "5000" defaultTCPParameters
  case t of
    Right transport -> do
      node <- newLocalNode transport rt
      rethrowProcess node runScript
      
    Left err -> Lua.luaerror $ "Error creating node on " ++ ip ++ ":" ++ show port
  
  where
    
    runScript = do
      env <- ask
      
      loggerPid <- spawnLocal $ forever $ do  
        (time, process, msg) <- expect :: Process SSH.LogMessage
        liftIO $ putStrLn $ printf "%s -> %s" (show process) msg  
               
      reregister "logger" loggerPid  
      
      
      liftIO $ do  
        loadNodeInterface sc env l        
        Lua.pushvalue l i
        tryCall l 0 0
        return ()        
        
distInterface :: RemoteTable -> SpawnClosure -> LuaState -> M.Map String Function
distInterface rt sc l = M.fromList $ 
  [ ("getInterfaces",     Function getInterfaces) 
    , ("doIdentity",      Function doIdentity)
    , ("codeIdentity",    Function codeIdentity)
    , ("stackIdentity",   Function stackIdentity)
    , ("spawnLocalNode",  Function $ spawnLocalNode rt sc l)
  ]
  
sshInterface :: LuaState -> M.Map String Function
sshInterface l = M.fromList $ 
    [ ("scanHosts",         Function $ scanHosts)
    , ("testHosts",         Function $ testHosts)
    , ("updateFiles",       Function $ updateFiles)

    , ("defaultSSHParams",  Function $ SSH.defaultSSHParams)
    ]
  

      
  
  
runLua :: RemoteTable -> SpawnClosure -> FilePath -> IO ()
runLua rt sc file = withLua rt sc $ \l -> do 
  r <- Lua.trylua $ doFile l file
  case r of 
    Right _ -> return ()
    Left  err -> putStrLn err
   


loadNodeInterface :: SpawnClosure -> LocalProcess -> LuaState ->  IO ()
loadNodeInterface sc env l = Lua.push l (nodeInterface sc env l) >> Lua.setglobal l "process" 

loadInterface :: RemoteTable -> SpawnClosure -> LuaState -> IO ()
loadInterface rt sc l = do
  Lua.callproc l "require" "torch"
  Lua.callproc l "require" "torchffi"
  
  doString l luaSerialize "luaSerialize"
  
  makeBinaryType l (undefined :: NodeId)
  makeBinaryType l (undefined :: ProcessId)
  
  Lua.push l (distInterface rt sc l) >> Lua.setglobal l "dist"
  Lua.push l (sshInterface l) >> Lua.setglobal l "ssh"  



