module Main where

import Scripting
import Interface

import qualified Scripting.Lua as Lua

import Control.Monad
import CmdLine
        
import qualified Control.Exception as E        
  
import qualified Backend.SSH as SSH

localMain :: IO ()
localMain = withCmdArgs $ \args -> do
  runLua (configFile args)

  
main :: IO ()
main = E.catch
        (SSH.withLocalMain remoteTable (appendFile "log.txt" . show) localMain)
        (\ (e :: E.SomeException) -> print e) 
      