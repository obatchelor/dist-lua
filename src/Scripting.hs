{-# LANGUAGE ExistentialQuantification, TemplateHaskell #-}

module Scripting 
  ( tryRun
  , tryCall
  , maybeCall
  , loadScript
  , loadString
  , doFile
  , doString
  
  , luaSerialize
  
  , torchType
  , assertTorch
  
  , maybeTagged
  , toString
    
  , module Scripting.Lua.Instances  
  , Lua.LuaState(..)
  
  , Function(..)
  , HsBinary(..)
  , LuaBinary(..)
  
  , pushBinary
  , peekBinary
  
  , _hsBinary
 
  , StackIndex(..)
  , LuaCode (..)
  , Field (..)
  
  , peekRecord
  , pushRecord
  
  , peekRecordDefault
  , allOptional
  , allRequired
  
  , getField  
  , setField

  , makeClass
  , makeBinaryType
  
  ) where

  
import Scripting.Lua.Instances

import Data.Char
import Data.Binary

  
import Foreign.C
import Foreign.Ptr  
import Foreign.ForeignPtr
import Foreign.Storable
import Data.Word  
import Data.Int
import Data.Maybe
import Data.List

import qualified Data.Map as M

import Control.Exception
import Control.Monad
import Control.Applicative
import Control.Lens

import qualified Scripting.Lua as Lua
import Scripting.Lua (setglobal, StackValue, LuaState, callfunc, callproc, getglobal2, pcall, pushvalue)

import qualified Data.Vector.Storable as V
import qualified Data.Vector.Storable.Mutable as MV

import qualified Data.ByteString.Lazy as BL

import qualified Data.ByteString as B
import Data.ByteString (ByteString)
import qualified Data.ByteString.Internal as B

import Data.Typeable 
import Control.Distributed.Process.Serializable  

import Text.Heredoc

tryRun :: Lua.LuaState -> IO Int -> IO ()         
tryRun l f = do 
  code <- f
  case code of 
    0 -> return ()
    _ -> do msg <- Lua.tostring l (-1)
            Lua.luaerror msg         

               
tryCall :: Lua.LuaState -> Int -> Int -> IO ()         
tryCall l numArgs numResults = do
  r <- maybeCall l numArgs numResults
  maybe (return ()) Lua.luaerror  r
  
  

printStack :: Lua.LuaState -> IO ()
printStack l = do
  
  top <- Lua.gettop l
  forM_ [1..top] $ \i -> do
    t  <- Lua.ltype l i
    p <-  Lua.topointer l i
    print (i, t, p)
    

  
maybeCall :: Lua.LuaState -> Int -> Int -> IO (Maybe String)       
maybeCall l numArgs numResults = do 
  
  getglobal2 l "debug.traceback"
  
  -- Load debug.traceback behind arguments, behind the function 
  trace <- globalIndex l (-numArgs -2) 
  Lua.insert l trace
  
  code <- Lua.pcall l numArgs numResults trace
  Lua.remove l trace
  
  case code of 
    Lua.PCOK -> return Nothing
    _ -> do msg <- Lua.tostring l (-1)
            return (Just msg)
          
               
loadScript :: Lua.LuaState -> FilePath -> IO ()
loadScript l file = tryRun l (Lua.loadfile l file)

loadString :: Lua.LuaState -> String -> String -> IO ()
loadString l str name = tryRun l (Lua.loadstring l str name)

-- | Execute a Lua script file
doFile :: Lua.LuaState -> FilePath -> IO ()
doFile l file = do
  loadScript l file
  tryCall l 0 0
  
-- | Execute a Lua script file
doString :: Lua.LuaState -> String -> String -> IO ()
doString l str name = do
  loadString l str name
  tryCall l 0 0
  
  
data Field r = forall a. Lua.StackValue a => Field (Lens' r a) 

    
pushRecord :: [(String, Field r, Bool)] -> LuaState -> r -> IO ()
pushRecord elems l r = do
  let len = length elems + 1
  Lua.createtable l len 0

  forM_ elems $ \(k, Field lens, _) -> do
     Lua.push l k
     Lua.push l (r ^. lens)
     Lua.rawset l (-3)
     
expand3 :: c -> (a, b) -> (a, b, c)
expand3 z (x, y) = (x, y, z)

allOptional :: [(a, b)] -> [(a, b, Bool)]
allOptional = map (expand3 True)

allRequired :: [(a, b)] -> [(a, b, Bool)]
allRequired = map (expand3 True)    

peekRecordDefault :: [(String, Field r, Bool)] -> IO r -> LuaState -> Int ->  IO r
peekRecordDefault fields getDefault l ix = do
  def <- getDefault
  peekRecord fields def l ix

lookup3 :: Eq a => a -> [(a, b, c)] -> Maybe (a, b, c)
lookup3 x = find ((== x) . fst3)

fst3 :: (a, b, c) -> a
fst3 (a, _, _) = a
  
  
checkFields :: [String] -> LuaState -> Int -> IO ()
checkFields fields l i = do
  table <- Lua.peek l i 
  forM_ (M.toList table) $ \(k, Ignore) -> do
    when (not $ elem k fields) $ do
      Lua.luaerror $ "Unknown field: " ++ k


peekRecord :: forall r. [(String, Field r, Bool)] -> r -> LuaState -> Int ->  IO r
peekRecord fields r l ix = do
  i <- getIdx l ix 
  
  checkFields (map fst3 fields) l i
  r <- foldM (build i) r fields
  
  return r
    
  where
    
    field k = "converting field \"" ++ k ++ "\", "
    
    build i r (name, Field lens, needed) = Lua.addloc (field name) $ do
      
      Lua.getfield l i name          
      mv <- Lua.peek l (-1)
      Lua.pop l 1
            
      case (mv, needed) of 
        (Nothing, False)  -> Lua.luaerror $ "field not found, " ++ name 
        (Just v, _)       -> return (set lens v r)
        (_, _) ->  return r  
      
  

data Function = forall f. Lua.LuaImport f => Function { unFunc :: f }
  
  
instance Lua.StackValue Function where
  push l (Function f)   = Lua.pushhsfunction l f
  peek l ix             = error "Cannot get function from lua"
 

-- Placeholder data type to hold a stack index for lua function calls
newtype StackIndex = StackIndex { unStack :: Int } deriving (Show, Eq)



instance Lua.StackValue StackIndex where
  push l (StackIndex ix) = Lua.pushvalue l ix
  peek l ix              =  do
    i <- getIdx l ix
    return $ StackIndex i


data Ignore = Ignore deriving Show
    
instance Lua.StackValue Ignore where
  push l (Ignore) = Lua.pushnil l
  peek l ix              = return Ignore
    
  

newtype LuaCode = LuaCode { unCode :: LuaBinary  } deriving (Show, Binary, Typeable)


instance StackValue LuaCode where
  push l (LuaCode bin) = do
    Lua.push l bin
    assertTorch l (-1) "failed to convert to function" "function"
       
  peek l ix = do  
    assertTorch l ix "" "function"
    code <- Lua.peek l ix
    return $ LuaCode $ code
    
  
typenameindex :: LuaState -> Int -> IO String
typenameindex l n = Lua.ltype l n >>= Lua.typename l


class Storable a => Storage a where
  storageType :: a -> String
  

-- Lua values which are serialized (in Haskell)
newtype LuaBinary = LuaBinary { unLua :: ByteString } deriving (Show, Ord, Eq, Binary, Typeable) 

-- Haskell values which are serialized (in Lua)
newtype HsBinary a = HsBinary { unBinary :: a } deriving (Show, Ord, Eq)


_hsBinary :: Simple Iso a (HsBinary a)  
_hsBinary = iso HsBinary unBinary  
 

  
instance Storage Word8 where
    storageType _ = "torch.ByteStorage"
    
instance Storage Int8 where
    storageType _ = "torch.CharStorage"
    

-- | Compute the normalised index of a value
getIdx :: Lua.LuaState -> Int -> IO Int
getIdx l i
  | i < 0 = do
      top <- Lua.gettop l
      return $ top + i + 1
  | otherwise = return i

  
  
torchType :: Lua.LuaState -> Int -> IO String
torchType l ix = do
  i <- getIdx l ix
  
  getglobal2 l "torch.typename"
  Lua.asserttype l (-1) Lua.TFUNCTION  
    
  pushvalue l i  
  tryCall l 1 1 

  name <- Lua.peek l (-1)
  Lua.pop l 1
  
  case name of 
    Just name -> return name
    Nothing   -> typenameindex l ix
  
  
assertTorch :: Lua.LuaState -> Int -> String -> String -> IO ()
assertTorch l ix location expectedType = do
  i <- getIdx l ix 
  t <- torchType l i 
    
  when (t /= expectedType) $
    void $ Lua.luaerror  $ location ++ ", expected: " ++ expectedType ++ ", got: " ++ t
  
  

storageLength :: forall a. Storage a =>  Lua.LuaState -> a -> Int -> IO Int
storageLength l s ix = do 
  
  i <- getIdx l ix
  assertTorch l i "storageLength" (storageType (undefined::a))
  
  
  getglobal2 l (storageType s ++ ".size")
  pushvalue l i
  
  tryCall l 1 1
  size <- Lua.peek l (-1)
  Lua.pop l 1
  
  return size

    
storagePtr :: forall a. Storage a => Lua.LuaState -> Int -> IO (Ptr a) 
storagePtr l ix = do
  i <- getIdx l ix
  assertTorch l i "storagePtr" (storageType (undefined::a))
  
  getglobal2 l "torch.data"
  
  pushvalue l i
  
  tryCall l 1 1
  
  ptrPtr <- Lua.topointer l (-1)
  Lua.pop l 1

  peek (castPtr ptrPtr)
    
    
createStorage :: Storage a => Lua.LuaState -> a -> Int -> IO ()
createStorage l s len = do
  
  getglobal2 l (storageType s) 
  Lua.push l len
    
  tryCall l 1 1
  
  
  
   

    
fromPtrLen :: Storable a => Ptr a -> Int -> IO (V.Vector a)
fromPtrLen ptr len = do
  ptr' <- newForeignPtr_ ptr
  V.freeze $ MV.unsafeFromForeignPtr0 ptr' len
    
    
toVector :: ByteString -> V.Vector Int8
toVector b = V.unsafeFromForeignPtr (castForeignPtr ptr) offset len where
  (ptr, offset, len) = B.toForeignPtr b

fromVector :: V.Vector Int8 -> ByteString
fromVector v = B.fromForeignPtr (castForeignPtr ptr) offset len where
  (ptr, offset, len) = V.unsafeToForeignPtr v



toString :: LuaState -> Int -> IO String
toString l ix = do
  i <- globalIndex l ix
  getglobal2 l "tostring"
  
  Lua.pushvalue l i
  tryCall l 1 1
  popStack l
  
  
 
instance StackValue LuaBinary where
  push l (LuaBinary bs) = do

    getglobal2 l "distSerialize.fromStorage"
    Lua.push l bs
        
    tryCall l 1 1 
    
  peek l ix = do 
    i   <- getIdx l ix
        
    getglobal2 l "distSerialize.toStorage"
    Lua.pushvalue l i
    
    tryCall l 1 1 
    
    (bs :: ByteString) <- popStack l
    return (LuaBinary bs)
  
 
  
instance StackValue ByteString where
  push l bs = Lua.push l (toVector bs)   
  peek l ix = do 
    v <- Lua.peek l ix
    return (fromVector v)
  

instance StackValue BL.ByteString where
  push l bs = Lua.push l (BL.toStrict bs)   
  peek l ix = do 
    bs <- Lua.peek l ix
    return (BL.fromStrict bs)
   
  
  
    
instance Storage a => StackValue (V.Vector a) where
  push l v = do
    createStorage l (undefined :: a) (V.length v)
    ptr <- storagePtr l (-1)   
    ptr' <- newForeignPtr_ ptr
    
    mv <- V.unsafeThaw v
    let mv' = MV.unsafeFromForeignPtr0 ptr' (V.length v)
    MV.copy mv' mv
    
    
  peek l ix =  do      
    i   <- globalIndex l ix    
    assertTorch l i "" (storageType (undefined::a))
    
    len <- storageLength l (undefined :: a) i
    ptr <- storagePtr l i
    
    v <- fromPtrLen ptr len
    return v
  


typeName :: Typeable a => a -> String
typeName a = show . typeOf $ a
  
    
  
-- | StackValue instance for Tagged haskell values, serialized in Lua
instance Serializable a => Lua.StackValue (HsBinary a) where
  push l  = pushBinary l . unBinary
  peek l ix = HsBinary <$> peekBinary l ix

      
pushBinary :: Serializable a => LuaState -> a -> IO ()
pushBinary l x = do
  Lua.getglobal2 l (typeName x)
  Lua.push l (encode x)
  tryCall l 1 1 
  

peekBinary :: forall a. Serializable a => LuaState -> Int -> IO a
peekBinary l ix = do
  assertTorch l ix "" name
  bs <- getField l ix "value"
  return (decode bs)
    where
      name = show . typeOf $ (undefined::a) 

      
        
maybeTagged :: (Lua.StackValue a) => Lua.LuaState -> Int -> IO (Maybe a)
maybeTagged  l ix = do
  r <- Lua.trylua $ peekTagged l ix return
  case r of 
    Right tag -> return (Just tag)
    Left  _ -> return Nothing
   
  

           
decodeStrict :: Binary a => ByteString -> a
decodeStrict = decode . BL.fromStrict

   
   
getField :: StackValue a => LuaState -> Int -> String -> IO a
getField l ix field = Lua.addloc ("getting field '" ++ field ++ "': ") $ do
  i <- globalIndex l ix
  
  Lua.asserttype l i Lua.TTABLE
  
  Lua.push l field
  Lua.gettable l i

  popStack l    
    
setField ::  StackValue a => LuaState -> Int -> String -> a -> IO ()
setField l ix field v = Lua.addloc ("setting field '" ++ field ++ "': ") $ do
  i <- globalIndex l ix
  
  Lua.asserttype l i Lua.TTABLE 
  
  Lua.push l field
  Lua.push l v
    
  Lua.settable l i

    
   
makeClass :: LuaState -> String -> [(String, Function)] -> IO ()
makeClass l className methods = do
  Lua.getglobal2 l "torch.class"
  Lua.asserttype l (-1) Lua.TFUNCTION  
    
  Lua.push l className
  tryCall l 1 1 
  
  table <- Lua.gettop l
  
  forM methods $ \(field, f) -> 
    setField l table field f
    
  Lua.pop l 1
  
makeBinaryType :: forall a. (Show a, Serializable a) =>  LuaState -> a -> IO ()
makeBinaryType l a = makeClass l (typeName a) 
    [ ("__tostring", Function toString)
    , ("__init",     Function init)
    ]
    
  where
    
    toString :: StackIndex -> IO String
    toString (StackIndex self) = do
      v <- getField l self "value"
      return . show $ (decodeStrict v :: a)  
  
    init :: StackIndex -> ByteString -> IO ()
    init (StackIndex self) value = setField l self "value" value
      

  

luaSerialize = [here|
   distSerialize = {} 

  function distSerialize.toStorage (object)
    local file = torch.MemoryFile()

    file:writeObject(object)   
    return file:storage()
  end


  function distSerialize.fromStorage (storage)
    local file = torch.MemoryFile(storage)
    return file:readObject()
  end
|] 
 


