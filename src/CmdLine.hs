module CmdLine 
  ( DistLua (..)
  , withCmdArgs  
  )
  where

import System.Console.CmdArgs
import qualified System.Console.CmdArgs as C

import System.Directory

data DistLua = DistLua { configFile :: String } deriving (Show, Data, Typeable)

options = cmdArgsMode $ DistLua { configFile = "" &= help "Configuration file" }
         &= summary "distributed-lua 0.1"
         
         
withCmdArgs :: (DistLua -> IO ()) -> IO ()
withCmdArgs action = do 
  args <- cmdArgsRun options
   
  exists <- doesFileExist (configFile args)
  if exists
     then action args
     else do 
       putStrLn "Specify configuration file:"
       print options         