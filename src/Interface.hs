{-# LANGUAGE FlexibleContexts, ScopedTypeVariables #-}

module Interface where

import Scripting
import qualified Scripting.Lua as Lua

import Backend.SSH (NodeHost(..))  
import qualified Backend.SSH as SSH
import qualified Backend.Local as SSH


import Control.Distributed.Process
import Control.Distributed.Process.Closure  
import Control.Distributed.Process.Node (runProcess, newLocalNode, initRemoteTable, forkProcess)
import Control.Distributed.Process.Serializable  

import qualified Control.Exception as E

import qualified Interface.Internal as I
import qualified Interface.Dict as D

import qualified Control.Distributed.Process.Dict as Dict

        
  
spawnLua :: I.LuaCall -> Process ()
spawnLua call = do
  sc <- expect
  catches (I.runLuaProcess' sc call) [Handler handleLua, Handler handler]
  
  where
    handleLua (e::Lua.LuaException) = say (Lua.leMessage e)
    handler (e::E.SomeException) = say (show e)
        
        
remotable ['spawnLua]        
        
remoteTable :: RemoteTable -> RemoteTable
remoteTable = Dict.remoteTable . D.remoteTable . SSH.remoteTable . __remoteTable 

runLua :: FilePath -> IO ()
runLua = I.runLua (remoteTable initRemoteTable) $(mkStatic 'spawnLua)
